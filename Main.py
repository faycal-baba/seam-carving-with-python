import argparse
import cv2
import sys
from time import time
from PyQt5.QtWidgets import QApplication
import Image as im
import MyGui as gui


def reduceSizeOnMouseClick(event,x,y,flags,seamCarving):
    if event == cv2.EVENT_LBUTTONUP:
        height,width = seamCarving.image.shape[:2]
        loopH = height - x if height>x else 0
        loopW = width - y if width > y else 0
        for i in range(loopH):
            seamCarving.removeVertSeam()
            cv2.imshow('Display',seamCarving.image)
            cv2.waitKey(1)
        for i in range(loopW):
            seamCarving.removeHorzSeam()
            cv2.imshow('Display',seamCarving.image)
            cv2.waitKey(1)


def run(seamCarving):
    
    cv2.namedWindow("Display",cv2.WINDOW_NORMAL)
    cv2.setMouseCallback("Display",reduceSizeOnMouseClick,seamCarving)
    cv2.imshow("Display",seamCarving.image)
    
        
    




if __name__ == '__main__':
    
    #On parse la commande et reupere champ image
    ap = argparse.ArgumentParser()
    ap.add_argument("-i", "--image", required = True, help = "Path to the image")
    args = vars(ap.parse_args())
    image = cv2.imread(args["image"])
    
    #crée notre objet seamCarving 
    seamCarving = im.Image(image)

    #Petit jeu de teste 
    start = time()
    for i in range(20):
        seamCarving.removeVertSeam()
        cv2.imshow("image",seamCarving.image)
        cv2.waitKey(1)
        print(i)
    end = time()
    height,width = seamCarving.image.shape[:2]
    print("Les nouvelles dimensions {} * {}".format(height,width)) 
    print("fin 1, temps de calcul {}".format(end - start))
    cv2.imshow("image",seamCarving.image)
    print("Appuyer pour continuer")
    cv2.waitKey(0)

   
    start = time()
    for i in range(20):
        seamCarving.addVertSeam()
        cv2.imshow("image",seamCarving.image)
        cv2.waitKey(1)
        print(i)
    end = time()
    height,width = seamCarving.image.shape[:2]
    print("Les nouvelles dimensions {} * {}".format(height,width)) 
    print("fin 2, temps de calcul {}".format(end - start))
    cv2.imshow("image",seamCarving.image)
    print("Appuyer pour continuer")
    cv2.waitKey(0)
    start = time()
    for i in range(20):
        seamCarving.removeHorzSeam()
        cv2.imshow("image",seamCarving.image)
        cv2.waitKey(1)
        print(i)
    end = time()
    height,width = seamCarving.image.shape[:2]
    print("Les nouvelles dimensions {} * {}".format(height,width)) 
    print("fin 3, temps de calcul {}".format(end - start))
    cv2.imshow("image",seamCarving.image)
    print("Appuyer pour continuer")
    cv2.waitKey(0)
    start = time()
    for i in range(20):
        seamCarving.addHorzSeam()
        cv2.imshow("image",seamCarving.image)
        cv2.waitKey(1)
        print(i)
    end = time()
    height,width = seamCarving.image.shape[:2]
    print("Les nouvelles dimensions {} * {}".format(height,width)) 
    print("fin 4, temps de calcul {}".format(end - start))
    cv2.imshow("image",seamCarving.image)
    print("Appuyer pour continuer")
    cv2.waitKey(0)
   
   
   
   
   
   
   
    end = time()
    print("le temps de calcul {}".format(end - start))
   
    

        
    
   
    
    
  

  
 
   
   


