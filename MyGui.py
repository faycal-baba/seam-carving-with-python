import sys
from PyQt5 import QtCore
from PyQt5.QtWidgets import QApplication, QWidget, QLabel
from PyQt5.QtGui import QIcon, QPixmap, QImage

class App(QWidget):
    resized = QtCore.pyqtSignal()
    def __init__(self):
        super().__init__()
        self.title = 'Open Cv - Seam carving'
        self.seamCarving = None
        self.label = QLabel(self)
        self.left = 100
        self.top = 100
        self.resized.connect(self.resizeImage)

    def initUI(self,scImage):
        self.setWindowTitle(self.title)
        qimg = QImage(scImage.image.data, scImage.image.shape[1],scImage.image.shape[0],QImage.Format_RGB888)
        qpm = QPixmap.fromImage(qimg)
        self.seamCarving = scImage
        self.setGeometry(self.left, self.top, qpm.width(),qpm.height())
        self.label.setPixmap(qpm)
        self.show()

    def displayImage(self):
        qimg = QImage(self.seamCarving.image.data, self.seamCarving.image.shape[1],self.seamCarving.image.shape[0],QImage.Format_RGB888)
        qpm = QPixmap.fromImage(qimg)
        label = QLabel(self)
        label.setPixmap(qpm)
        self.show()
        print("reshow")
        QWidget.update(self)
        return None
    
    def resizeEvent(self, event):
       self.resized.emit()
       return super(App, self).resizeEvent(event) 

    def resizeImage(self):
        
        newHeight = self.frameGeometry().height()
        newWidth = self.frameGeometry().width()
        #print("resized"+str(self.frameGeometry().height())+str(self.frameGeometry().width()))
        height,width = self.seamCarving.image.shape[:2]
        loopV = width - newWidth 
        loopH = height - newHeight
        #print("new height =" + str(newHeight) + "\n new Width"+str(newWidth))
        if loopV > 0 :
            
            for i in range(loopV):
                self.seamCarving.removeVertSeam()
                self.displayImage()
                print('Vert removed' + str(i))
            QtCore.QCoreApplication.processEvents()
        elif loopV < 0:
            
            for i in range(loopV,-1):
                self.seamCarving.addVertSeam()
                self.displayImage()
            QtCore.QCoreApplication.processEvents()
        if loopH > 0:
            
            for i in range(loopH):
                self.seamCarving.removeHorzSeam()
                self.displayImage()
                print('Horz removed' + str(i))
            QtCore.QCoreApplication.processEvents()
        elif loopH < 0:
            for i in range(loopH,-1):
                QtCore.QCoreApplication.processEvents()
                self.seamCarving.addHorzSeam()
                self.displayImage()
            QtCore.QCoreApplication.processEvents()
        print("fin")
        
        return None  


        

        
        
