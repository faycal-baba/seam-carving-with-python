import cv2 as cv
import numpy as np
import matplotlib as plt
import seamClass as sc

class Image:
    
    def __init__(self,image):
        self.image = image
        self.removedVertSeams = []
        self.removedHorzSeams = []
        self.vertSeamToRemove = []
        self.horzSeamToRemove = []
    
       
    #converts the BGR image used in openCv to RGB Numpy
    def BGR2Numpy(self,image):
        b,g,r = cv.split(image)
        return cv.merge([r,g,b])
        
    #on applique un floutage de gauss afin d'eliminer les parasites dans l'image
    @staticmethod
    def gaussianBlur(img):
        return cv.GaussianBlur(img, (3, 3), 0, 0)

    #Convertir l'image en GrayScale (afin de reduire la complexité des calculs  )
    @staticmethod
    def toGrayScale(img):
        return cv.cvtColor(img, cv.COLOR_RGB2GRAY )

    #calcul gradient x avec Sobel 
    @staticmethod
    def gradientX(img):
        return cv.Sobel(img, cv.CV_64F, 1, 0, ksize=3,scale=1, delta=0, borderType=cv.BORDER_DEFAULT)
    #calcul gradient y
    @staticmethod
    def gradientY(img):
        return cv.Sobel(img, cv.CV_64F, 0, 1, ksize=3,
                        scale=1, delta=0, borderType=cv.BORDER_DEFAULT)

    #calcul energy 
    @staticmethod
    def energy(image):
        blurred = Image.gaussianBlur(image)
        grayScaledImage = Image.toGrayScale(blurred)
        gx = Image.gradientX(grayScaledImage)
        gy = Image.gradientY(grayScaledImage)
        # On peut soit retourner La racine carré de l'addition des deux gradients 
        # ou simpelment l'addition de leures valeurs absolues
        return cv.add(np.absolute(gx), np.absolute(gy))
    
    #calcul des energies mininim verticalement/ programation dynamique
    @staticmethod
    def totalEnergyVert(image):
        energyMat = Image.energy(image)
        height,width = energyMat.shape[:2]
        energiestotal = np.zeros((height,width))
        energiestotal[0,:] = energyMat[0,:]
        
        for i in range(1,height):
            for j in range(width):
                left = energiestotal[i-1,j-1] if  j-1 >= 0 else 1e6
                mid =  energiestotal[i-1,j]
                right =  energiestotal[i-1,j+1]  if  j+1 < width else 1e6

                energiestotal[i,j] = energyMat[i,j] + min(left,mid,right)
        return energiestotal
    #calcul des energies mininim Horizontale/ programation dynamique
    @staticmethod
    def totalEnergyHoriz(image):
        energyMat = Image.energy(image)
        height,width = energyMat.shape[:2]
        energiestotal = np.zeros((height,width))
        energiestotal[:,0] = energyMat[:,0]

        for i in range(1,width):
            for j in range(height):
                
                top = energiestotal[j-1,i-1] if j-1>=0 else 1e6
                mid = energiestotal[j,i-1] 
                bot = energiestotal[j+1,i-1] if j+1<height else 1e6

                energiestotal[j,i] = energyMat[j,i] + min(top,mid,bot)
        return energiestotal

    # recherche du seam d'energie min vertical
    @staticmethod
    def verticalSeam(image):
        totalEnergyMat = Image.totalEnergyVert(image)
        height,width = totalEnergyMat.shape[0:2]
        seam = []
        col = np.argmin(totalEnergyMat[height-1,:])
        seam.append([height-1,col])
        for i in range(height-2,-1,-1):
            row = totalEnergyMat[i,:]
            left = row[col-1] if col - 1 >=0 else 1e6
            mid = row[col] 
            right = row[col+1] if col + 1 < width else 1e6

            col = col - 1 + np.argmin([left,mid,right])
            seam.append([i,col])
        return seam

    #recherche d'un seam d'energie min Horizontal
    @staticmethod
    def horizontalSeam(image):
        totalEnergyMat = Image.totalEnergyHoriz(image)
        height,width = totalEnergyMat.shape[0:2]
        seam = []
        row = np.argmin(totalEnergyMat[:,width-1])
        seam.append([row,width-1])
        for i in range(width-2,-1,-1):
            
            col = totalEnergyMat[:,i]
            top = col[row-1] if row - 1>=0 else 1e6
            mid = col[row] 
            bot = col[row+1] if row + 1 < height else 1e6

            row = row - 1 + np.argmin([top,mid,bot])
            seam.append([row,i])
        return seam


    #effacer un liste de pixels de l'image Vertical
    def removeVertSeam(self):
        height, width, BGR = self.image.shape
        reducedImage = np.zeros((height, width - 1, BGR), np.uint8)
        seamToRemove = Image.verticalSeam(self.image)
        seam = sc.Seam()
        print("About to remove vertical Seam")
        for x, y in seamToRemove[::-1]:
            reducedImage[x, 0:y] = self.image[x, 0:y]
            seam.add([x,y],self.image[x,y])
            reducedImage[x, y:width - 1] = self.image[x, y + 1:width]
        self.removedVertSeams.append(seam)
        self.image = reducedImage
        return None
   

    #effacer une liste de pixel de l'image horizontal
    def removeHorzSeam(self):
        height,width,BGR = self.image.shape
        reducedImage = np.zeros((height - 1,width,BGR),np.uint8)
        seamToRemove = Image.horizontalSeam(self.image)
        seam = sc.Seam()
        print("About to remove horizontal Seam")
        for x,y in seamToRemove[::-1]:
            reducedImage[0:x,y] = self.image[0:x,y]
            seam.add([x,y],self.image[x,y])
            reducedImage[x:height-1,y] = self.image[x+1:height,y]
        self.removedHorzSeams.append(seam)
        self.image = reducedImage
        return None

    
    #Ajoute un seam vertical à l'image
    def addVertSeam(self):
        if self.removedVertSeams:
            height,width,BGR = self.image.shape
            newImg = np.zeros((height,width + 1,BGR),np.uint8)
            seam = self.removedVertSeams[-1] if self.removedVertSeams else sc.Seam()
            self.removedVertSeams = self.removedVertSeams[:-1]
    
            for indice,BGR in zip(seam.indices,seam.BGR):
                newImg[indice[0],:indice[1]] = self.image[indice[0],:indice[1]]
                newImg[indice[0],indice[1]] = BGR
                newImg[indice[0],indice[1]+1:] = self.image[indice[0],indice[1]:]
            self.image = newImg
        
        return None
    #Ajoute un seam horizontal à l'image
    def addHorzSeam(self):
        if self.removedHorzSeams:
            height,width,BGR = self.image.shape
            newImg = np.zeros((height + 1,width,BGR),np.uint8)
            seam = self.removedHorzSeams[-1] if self.removedHorzSeams else sc.Seam()
            self.removedHorzSeams = self.removedHorzSeams[:-1]

            for indice,BGR in zip(seam.indices,seam.BGR):
                newImg[:indice[0],indice[1]] = self.image[:indice[0],indice[1]]
                newImg[indice[0],indice[1]] = BGR
                newImg[indice[0]+1:,indice[1]] = self.image[indice[0]:,indice[1]]
            self.image = newImg
        return None

   
    def highlightSeam(self,seam):
        newImg = self.image
        
        for x,y in seam:
            newImg[x,y] = [255,0,0] 
        return newImg       
        
        
   
            

    
            
     
                    
                    
                    


                    

            


        
        


   


    
