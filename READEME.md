## UPEC 2017/2018
#### RAPPORT PROJET PYTHON
#### Seam carving avec Python

_Groupe_ : 
>&nbsp;&nbsp;&nbsp; BABA Faycal  
&nbsp;&nbsp;&nbsp; Matricule : **U21717296**

---


**Parlons du projet**  :   
<pre>  Le projet consiste donc à impelementer en Python l'algorithme Seam carving, qui permet de redimensionner une image de façon intelligente et cela en effaçant des Seams, ou autremant dit, des chemins de pixels dits de moindre importance.  
---
**Implementation**  

* Le programme effectue un redimensionnement vertical et horizontal de l'image mise en entrée 
* Commande d'execution :  
>Afin de lancer le programme il suffit d'ecrire la commande:   

             <pre> Python Main.py --image CheminVerslimage 


* Environnement de travail, Outils et FrameWork  :   
  &nbsp;&nbsp;&nbsp; Système d'exploitation :  Windows 10 64bits  
  &nbsp;&nbsp;&nbsp; Python : version 3.6.3 64bits  
  &nbsp;&nbsp;&nbsp; Editeur : Visual studio code  
  &nbsp;&nbsp;&nbsp; Libraries : OpenCv 3.4.0, Numpy, Matplotlib, PyQT5  


---

* Fonction d'energie :  

 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Avant de calculer l'energie on applique d'abord un filtre de gauss afin de reduire le bruits "Noise" dans l'image grace à une methode d'OpenCv "Gaussian_blur"
puis on convertis m'image en grayscale afin de reduire la complexité des calculs pour la fonction d'energie.  

  |La formule utilisée pour le calcul d'energie : 
---
    Energie = |Gradient X| + |Gradient Y|
---

tel que  les les gradients sont calculés avec la fonction sobel d'OpenCv, pour plus d'information voir [ce lien](https://docs.opencv.org/2.4/doc/tutorials/imgproc/imgtrans/sobel_derivatives/sobel_derivatives.html) 

enuite, on calcule les energies cumulées en prenant à chaque fois l'energie minimum des 3 pixels adjacents du haut pour cas vertical et de gauche pour l'horizontal, ceci génère une matrice avec dans chaque case l'energie minimum cumulée à partir des pixels précédents. On repart donc à partir de la dernière ligne/colonne en choisissant à chaque fois le parent d'energie minimale afin de trouver le meilleur chemin.

---  

* Exemple :  
Sur l'image loutres 640 * 480 on va proceder à reduire 25 seams verticalement et horizontalement:  

![](ResizeOfOtaries.gif) 

le temps de calcul de toutes l'operation est de 42 secondes

un autre exemple sur image relativement plus petite:
l'image :

![](ResizeOfPaysage.gif) 

le temps de calcul pour cette image de dimension 259 * 194 est de 7 secondes  

---

* Bug et Problèmes  
Ayant eu des problèmes au niveau de l'interface graphique et la gestion de l'evenement  **_resize_**, le lancement du programme ne fait qu'un jeu de teste qui consiste à enlever 20 seam verticaux puis les remettre et meme chose horizontalement
---
* Documentation : 

[Open Cv pour python](https://www.programcreek.com/python/index/2663/cv2)  
[Support pour l'interface graphique](https://pythonspot.com/en/pyqt5-image/)  
[Cours sur python et Numpy + Matplotlib sur DataCamp](https://www.datacamp.com/courses/python-data-science-toolbox-part-1)  
et les eternels classiques Youtube + Wikipédia 



